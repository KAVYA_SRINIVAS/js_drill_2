function currentMonth(cur_date){
    if(cur_date.length!==10){
        return 'Invalid Date Format, Please follow dd/mm/yyyy format';
    }else{
        for(let i=0;i<cur_date;i++){
            if(cur_date[i]<47 || cur_date[i]>57)
            return 'Date cannot have alphabets or any special characters except "/"';
        }

    }

    let mm=cur_date.substr(3,2);
    //console.log(mm);
    if(!((mm)>47) && !((mm<58))){
        return 'Date cannot have alphabets or any special characters except "/"';
    }
    switch(mm){
        case '01':
            return 'January';
            break;
        case '02':
            return 'February';
            break;
        case '03':
            return 'March';
            break;
        case '04':
            return 'April';
            break;
        case '05':
            return 'May';
            break;
        case '06':
            return 'June';
            break;
        case '07':
            return 'July';
            break;     
        case '08':
            return 'August';
            break;
        case '09':
            return 'September';
            break;
        case '10':
            return 'October';
            break;
        case '11':
            return 'November';
            break;
        case '12':
            return 'December';
            break;
        default :
            return 'Invalid Month';
            break;
    }
}

module.exports.cur_Month=currentMonth;