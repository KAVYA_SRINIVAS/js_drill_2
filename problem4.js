function titleCase(nameObj){
    const name=Object.values(nameObj);
    let str="";
    for(let i=0;i<name.length;i++){
        name[i]=name[i].toLowerCase();
        str=str + " " +name[i].charAt(0).toUpperCase()+name[i].substr(1);
    }
    return str;
}

module.exports.title=titleCase;

/* let t1={"first_name": "JoHN", "last_name": "SMith"};
let t2={"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"};
let t3={"first_name": "KaVyA", "last_name": "SRInIvAs"};

console.log(titleCase(t1));
console.log(titleCase(t2));
console.log(titleCase(t3)); */