const findMonth=require('../problem3');

const test1=findMonth.cur_Month('14/12/1997');
console.log(test1);

const test2=findMonth.cur_Month('14/2/1997');
console.log(test2);

const test3=findMonth.cur_Month('helloworld');
console.log(test3);

const test4=findMonth.cur_Month('12/de/2020');
console.log(test4);

const test5=findMonth.cur_Month('12/30/2020');
console.log(test5);